<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Link;
use App\Message;
use Auth;
use Carbon\Carbon;

class MessageController extends Controller
{
	public function __construct(Link $linkFactory)
	{
		$this->linkFactory = $linkFactory;
	}

	public function make(Request $request, Link $linkFactory)
	{
		$link = $linkFactory->make();
		$limit = ((int) $request->limit) * 60;

		$message = new Message;

		$message->fill($request->all()); /// !!!

		$message->message = $request->message;
		$message->title = $request->title;
		$message->language = $request->language;
		$message->access = (int)$request->access; // 0 - private, 1 - unlisted, 2 - public
		$message->ends_at = $limit > 0 ? Carbon::now()->addSeconds($limit) : null;
		if (Auth::check())
			$message->user_id = Auth::user()->id;
		$message->link = $link;
		$message->save();

		return view('link')->with('link', $link);
	}

	public function getMessage($link)
	{
		$user_id = Auth::check() ? Auth::user()->id : -1;
		$message = Message::where('link', $link)->first();
		$messageRespone = [
			'message' => $message->message,
			'title' => $message->title,
			'language' => $message->language
		];

		// return view('...', $message); // !!!

		if (!$message->ends_at || $message->ends_at > Carbon::now()) {
			if ($message->access == 0 && $user_id == $message->user_id) {
				return view('messageShow')->with('message', $messageRespone);
			}
			return $message->access != 0 ? view('messageShow')
			->with('message', $messageRespone)
			: 'Сообщение не существует';
		}
		return 'Сообщение не существует';
	}

	public function maker()
	{
		return view('messageMake');
	}

}
