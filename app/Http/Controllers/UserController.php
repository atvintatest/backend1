<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use Auth;

class UserController extends Controller
{
    public function show()
    {
		$user = Auth::user();
    	$messages = Message::where('user_id', $user->id)->paginate(10);
        $messageArray = [];
        foreach ($messages as $key => $value) {
            $messageArray[$key]['message'] = $value->message;
            $messageArray[$key]['link'] = $value->link;
        };
		return view('profile')->with('messages', $messageArray);
    }
}
