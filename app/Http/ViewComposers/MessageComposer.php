<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Message;
use Auth;
use Carbon\Carbon;
class MessageComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $myMessages = null;
        $publicMessages = Message::where([
              ['access', 2],
              ['ends_at', '>', Carbon::now()]
          ])->orWhere([
              ['access', 2],
              ['ends_at', null]
          ])
            ->orderBy('created_at', 'desc')
            ->take(10)
            ->get();
        if (Auth::check()) {
            $myMessages = Message::where([
                    ['user_id', Auth::user()->id],
                    ['ends_at', '>', Carbon::now()],
                ])->orWhere([
                    ['user_id', Auth::user()->id],
                    ['ends_at', null]
                ])
                ->orderBy('created_at', 'desc')
                ->take(10)
                ->get();
        }
        $view->with('lastTen', $publicMessages)->with('myTen', $myMessages ? $myMessages : null);
    }
}
