<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

	protected $fillable = [
		'message', 'link', 'user_id', 'ends_at', 'title', 'language'
	];
}
