<?php

namespace App\Modules;

class Link {

  public function make()
  {
      $aphabet = 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890';
      $link = '';
      for ($i = 0; $i < 8; $i++) {
          $r = rand(0, 62);
          $link .= substr($aphabet, $r, 1);
      }

      return $link;
  }
}
