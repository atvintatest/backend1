<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Messages extends Migration
{
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
          //$table->integer('id')->unsigned()->primary();
            $table->increments('id')->unique();
            $table->text('message');
            $table->string('title')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('access');
            $table->dateTime('ends_at')->nullable();
            $table->string('language')->nullable();
            $table->string('link');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
