
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Message</div>
					<input type="text" class="form-control" readonly value="backend/message/{{ $link }}">
          	</div>
        </div>
    </div>
</div>
@endsection
