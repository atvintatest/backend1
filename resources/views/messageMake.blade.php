@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Message</div>
				<form role="form" method="post" action="/message">
					{{ csrf_field() }}
          <div class="form-group">
            <input class="form-control" type="text" id="title" name="title" placeholder="Название"></textarea>
          </div>
					<div class="form-group">
						<textarea class="form-control" rows="5" id="message" name="message" placeholder="Message"></textarea>
					</div>
          <div class="form-group">
            Длительность
						<select class="form-control" id="limit" name="limit">
              <option value="10">10 минут</option>
              <option value="60">1 час</option>
              <option value="180">3 часа</option>
              <option value="1440">1 день</option>
              <option value="10080">1 неделя</option>
              <option value="44640">1 месяц</option>
              <option value="0">Без ограничения</option>
            </select>
					</div>
          <div class="form-group">
            Доступ
            <select class="form-control" id="access" name="access">
               @if (Auth::user())
              <option value="0">Только мне</option>
              @endif
              <option value="1">Только по ссылке</option>
              <option value="2">Свободный</option>
            </select>
          </div>
          <div class="form-group">
            Язык
            <select class="form-control" id="language" name="language">
              <option value=""></option>
              <option value="csharp">C#</option>
              <option value="JavaScript">JavaScript</option>
              <option value="CSS">CSS</option>
            </select>
          </div>
					<button type="submit" class="btn btn-default">Отправить</button>
				</form>
            </div>
        </div>
    </div>
</div>
@endsection
