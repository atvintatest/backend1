@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $message['title'] }}</div>
                <div class="panel-body">
                  <pre><code class="{{ $message['language']}}">{{ $message['message'] }}</code></pre>

                </div>
            </div>
        </div>
        
    </div>
</div>
@endsection
