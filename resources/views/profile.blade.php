@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Ваши сообщения</div>
                <table class="table">
                    @foreach ($messages as $message)
                    <tr>
                        <td style="word-break: break-all; width: 65%">
                            {{ $message['message'] }}
                        </td>
                        <td style="word-break: break-all; width: 35%">
                            localhost/message/{{ $message['link'] }}
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
