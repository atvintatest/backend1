<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'MessageController@maker');


//messages
Route::get('/message', 'MessageController@maker');
Route::post('/message', 'MessageController@make');
Route::get('/message/{link}', 'MessageController@getMessage');
//User
Route::get('/profile', [ 'middleware' => 'auth', 'uses' => 'UserController@show' ]);
Route::get('auth/vk', 'OauthController@vkontakte');
